import boto3
import logging
import json
import os
import random

logger = logging.getLogger()

coops = [
    "Animus",
    "Bantics",
    "Bitson",
    "Camba",
    "Free code",
    "Free colective",
    "Coprinf",
    "Devecoop",
    "Equality",
    "Eryx",
    "Fiqus",
    "Gaia",
    "Gcoop",
    "Geneos",
    "It10",
    "Nayra",
    "Slam",
    "Tecso",
    "Unixono",
    "The cornfield",
    "Tera",
    "Rook"
]

def lambda_handler(event, context):
    print(event)
    try:
        status_code = 200
        body = random.choice(coops) #

    except Exception:
        body = "Something went wrong\n"
        logger.exception(body)
        status_code = 500

    return {
        "statusCode": status_code,
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": "true",
        },
        "body": json.dumps(body),
    }
