variable "region" {
  default = "us-east-2"
}

variable "credentials" {
  default = "~/.aws/credentials"
}

variable "profile" {
  default = "rook"
}

variable "stage" {
  default = "dev"
}

variable "project" {
  default = "tf-puntapie-inicial"
}

variable  "tags"  {
  type        = map(string)
  default = {
    State       = "terraform managed"
    Environment = "tf-puntapie-inicial-dev"
  }
}
