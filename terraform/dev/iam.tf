# See also the following AWS managed policy: AWSLambdaBasicExecutionRole
resource "aws_iam_policy" "iampolicy_logs" {
    name        = "${var.project}-${var.stage}-logs"
    path        = "/"
    description = "IAM policy for logging from a lambda"

    policy = file("../policies/logs.json")
}