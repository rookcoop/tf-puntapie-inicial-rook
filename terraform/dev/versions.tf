terraform {

    required_version = "~> 0.12.29"

    required_providers {
        aws     = "~> 3.9.0"
        archive = "~> 2.0"
        null    = "~> 3.0"
    }
}