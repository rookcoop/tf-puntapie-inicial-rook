resource "aws_s3_bucket" "web_app_container" {
    bucket_prefix = "${var.project}-${var.stage}"
    acl    = "public-read"

    website {
        index_document = "index.html"
        error_document = "index.html"
    }

    tags = merge(var.tags, { Name = "web-app-container" })
}

resource "aws_s3_bucket_policy" "web_app_container_policy" {
    bucket = aws_s3_bucket.web_app_container.id

    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
        "Sid": "AddPerm",
        "Effect":"Allow",
        "Principal": "*",
        "Action": "s3:GetObject",
        "Resource": "${aws_s3_bucket.web_app_container.arn}/*"
        }
    ]
}
POLICY
}

locals {
    web_build_path = "../../frontend/build"
}

resource "aws_s3_bucket_object" "web_app" {
    for_each = fileset(local.web_build_path, "**")

    bucket       = aws_s3_bucket.web_app_container.id
    key          = each.value
    source       = "${local.web_build_path}/${each.value}"
    content_type = length(regexall("^.*\\.(.*)", each.value)) > 0 ? lookup(local.extension_to_mime, element(regex("^.*\\.(.*)", each.value), 0), null) : null
    etag         = filemd5("${local.web_build_path}/${each.value}")
}