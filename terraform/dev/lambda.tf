module "lambda_random_choice" {
    source = "git::https://github.com/rooktechcoop/aws-lambda-basic.git"

    lambda_function_name = "${var.project}-${var.stage}-random-choice"
    lambda_code_path     = "../../lambdas/random_choice"
    lambda_handler       = "lambda_function.lambda_handler"
    lambda_runtime       = "python3.8"
    lambda_policy_arn    = [ aws_iam_policy.iampolicy_logs.arn ]
    lambda_description   = "Random choice lambda"
    lambda_timeout       = "60"

    tags = var.tags
}