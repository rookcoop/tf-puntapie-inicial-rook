output "apigw_name" {
  value = aws_api_gateway_rest_api.apigw.name
}

output "frontend_endpoint" {
  value = aws_s3_bucket.web_app_container.website_endpoint
}