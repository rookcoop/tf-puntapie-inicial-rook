provider "aws" {
    region                  = var.region
    shared_credentials_file = var.credentials
    profile                 = var.profile
}

terraform {
    backend "s3" {
        bucket         = "tf-puntapie-inicial-rs"
        encrypt        = "true"
        dynamodb_table = "tf-puntapie-inicial-rs"
        key            = "tf-puntapie-inicial-rs-dev.tfstate"
        region         = "us-east-2"
        profile        = "rook"
    }
}

data "aws_caller_identity" "current" {}

data "aws_region" "current" {}