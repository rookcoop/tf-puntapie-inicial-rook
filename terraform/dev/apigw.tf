resource "aws_api_gateway_rest_api" "apigw" {
  name        = "${var.project}-${var.stage}-apigw"
  description = "API gateway"
}

resource "aws_api_gateway_resource" "api_gateway_resource_random_choice" {

  rest_api_id = aws_api_gateway_rest_api.apigw.id
  parent_id   = aws_api_gateway_rest_api.apigw.root_resource_id
  path_part   = "random_choice"
}

module "apigw_trigger_hello_world" {

  source = "git::https://github.com/rooktechcoop/aws-lambda-apigw-trigger?ref=custom"
  module_dependency = join(",", [module.lambda_random_choice.module_complete])

  lambda_function_name         = module.lambda_random_choice.lambda_name
  lambda_invoke_uri_arn        = module.lambda_random_choice.lambda_invoke_uri_arn
  api_gateway_id               = aws_api_gateway_rest_api.apigw.id
  api_gateway_resource_id      = aws_api_gateway_resource.api_gateway_resource_random_choice.id
  api_gateway_resource_path    = aws_api_gateway_resource.api_gateway_resource_random_choice.path
  request_method               = "GET"
  stage_name                   = "dev"
  cors_enable                  = true
}