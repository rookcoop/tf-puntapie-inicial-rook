variable "region" {
  type    = string
  default = "us-east-2"
}

variable "credentials" {
  type    = string
  default = "~/.aws/credentials"
}

variable "profile" {
  type    = string
  default = "rook"
}

variable "project" {
  type    = string
  default = "tf-puntapie-inicial"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "name" {
  type    = string
  default = "rs" 
}

