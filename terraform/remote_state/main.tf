provider "aws" {
    region                  = var.region
    shared_credentials_file = var.credentials
    profile                 = var.profile
}

module "remote_state" {
    source  = "../modules/remote-state"

    aws_region                  = var.region
    aws_environment             = var.environment
    s3_bucket_name              = "${var.project}-${var.name}"
    dynamodb_table_name         = "${var.project}-${var.name}"
    iam_group_name_ro_access    = "${var.name}-${var.project}-terraform-ro-access"
    iam_group_name_rw_access    = "${var.name}-${var.project}-terraform-rw-access"
}
