import React from 'react';

import { Typography, Row, Col, Button, Card } from 'antd'

const axios = require('axios')

const { Title } = Typography

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      name: ""
    }
  }

  callBackend() {
    this.setState({ loading: true, name: "" })
    axios.get('https://vlpxqxkkki.execute-api.us-east-2.amazonaws.com/dev/random_choice')
    .then(response => {
      this.setState({
        loading: false,
        name: response.data
      })
    })
    .catch(console.log)
  }

  render() {
    return (
      <>
        <Title style={{ textAlign: "center" }} >
          FACTTIC DEMO
        </Title>
        <Card style={{ margin: "15px", backgroundColor: "purple" }} >
          <Title level={3} style={{ textAlign: "center", color: "white" }} >
            Random coop picker
          </Title>
          <Row>
            <Col span={12} style={{ textAlign: "center" }} >
              <Button
                loading = {this.state.loading}
                onClick = {() => this.callBackend()}
              >
                Get coop!
              </Button>
            </Col>
            <Col span={12} style={{ textAlign: "center" }}>
              <Title level={5} style={{ color: "white" }}>{this.state.name}</Title>
            </Col>
          </Row>
        </Card>
      </>
    );
  }
}

export default App;
